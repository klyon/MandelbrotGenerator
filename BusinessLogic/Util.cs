﻿using MandelbrotGenerator.Model;
using System.Drawing;
using System.Numerics;

namespace MandelbrotGenerator.BusinessLogic
{
    internal class Util
    {
        /// <summary>
        /// Calculates the size to draw the new rectangle, based on the
        /// size of the current image, the resolution of the new image
        /// and the zoom factor.
        /// </summary>
        /// <param name="currentSize">The resolution of the current image</param>
        /// <param name="newSize">The resolution of the next image</param>
        /// <param name="zoomFactor">The desired zoom factor</param>
        /// <returns></returns>
        internal static Size GetRectagleSize(Size currentResolution, Size newResolution, int zoomFactor)
        {
            double rectWidth = 0;
            double rectHeight = 0;

            if (currentResolution == newResolution)
            {
                rectWidth = currentResolution.Width / (double)zoomFactor;
                rectHeight = currentResolution.Height / (double)zoomFactor;
            }
            else
            {
                var curAspectRatio = currentResolution.Width / (double)currentResolution.Height;
                var newAspectRatio = newResolution.Width / (double)newResolution.Height;

                if (newAspectRatio > curAspectRatio)
                {
                    // Use width to scale
                    rectWidth = currentResolution.Width / (double)zoomFactor;
                    rectHeight = rectWidth / newAspectRatio;
                }
                else
                {
                    // Use height to scale
                    rectHeight = currentResolution.Height / (double)zoomFactor;
                    rectWidth = rectHeight * newAspectRatio;
                }
            }

            return new Size((int)rectWidth, (int)rectHeight);
        }
    }
}
