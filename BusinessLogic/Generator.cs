﻿using MandelbrotGenerator.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace MandelbrotGenerator.BusinessLogic
{
    public class Generator
    {
        private const double _limit = 2;

        /// <summary>
        /// The default area of the complex plane to show the Mandelbrot Set.
        /// </summary>
        public static readonly SizeF DefaultBounds = new SizeF(3, 2);

        public event EventHandler<EventArgs> Progress;

        public PixelInfo[,] Result { get; private set; }

        private double _xStep = 0;

        private double _yStep = 0;

        public async Task GenerateSet(GeneratorSettings generatorSettings, bool async = true)
        {
            var defaultAspectRatio = Generator.DefaultBounds.Width / Generator.DefaultBounds.Height;

            // New logic
            var xMin = generatorSettings.Area.Left;
            var xMax = generatorSettings.Area.Right;
            var yMax = generatorSettings.Area.Top;
            var yMin = generatorSettings.Area.Bottom;

            _xStep = Math.Abs(xMax - xMin) / (generatorSettings.Resolution.Width - 1);
            _yStep = Math.Abs(yMin - yMax) / (generatorSettings.Resolution.Height - 1);

            Result = new PixelInfo[generatorSettings.Resolution.Width, generatorSettings.Resolution.Height];

            // Separate the area of the image into horizontal stripes, where each thread tackles an area.
            int threadCount = Environment.ProcessorCount;
            var batchHeight = (int)(generatorSettings.Resolution.Height / threadCount);

            List<BatchInfo> batchInfos = new List<BatchInfo>();

            // Determine what screen area should be handled by each thread.
            for (int i = 0; i < threadCount; i++)
            {
                BatchInfo batchInfo = new BatchInfo();
                batchInfo.ScreenArea.Y = (i * batchHeight);
                batchInfo.ScreenArea.Width = generatorSettings.Resolution.Width;
                batchInfo.ScreenArea.Height = batchHeight;
                batchInfo.TopLeft = new Complex(xMin, yMax - (i * batchHeight * _yStep));
                batchInfo.MaxIterations = generatorSettings.MaxIterations;
                batchInfos.Add(batchInfo);
            }

            if (batchInfos.Last().ScreenArea.Bottom < generatorSettings.Resolution.Height)
            {
                // Increase the last area to correct any rounding error
                // from splitting the height unevenly.
                var lastItem = batchInfos.Last();
                lastItem.ScreenArea.Height = (generatorSettings.Resolution.Height - lastItem.ScreenArea.Top);
            }

            Action<object> action = (object batchInfo) => ProcessBatch(batchInfo);
            Task[] batchTasks = new Task[batchInfos.Count];

            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < batchInfos.Count; i++)
            {
                batchTasks[i] = Task.Factory.StartNew((arg) =>
                    {
                        int batchInfoIndex = (int)arg;
                        ProcessBatch(batchInfos[batchInfoIndex]);
                    },
                    i,
                    TaskCreationOptions.LongRunning);
            }

            // Wait for all the threads to finish.
            if (async)
                await Task.WhenAll(batchTasks);
            else
                Task.WaitAll(batchTasks);

            sw.Stop();
            Debug.WriteLine("Duration: " + sw.Elapsed.ToString());
        }

        private void ProcessBatch(object batchInfoObj)
        {
            var batchInfo = (BatchInfo)batchInfoObj;

            double real;
            double imaginary = batchInfo.TopLeft.Imaginary;

            for (int y = batchInfo.ScreenArea.Top; y < batchInfo.ScreenArea.Bottom; y++)
            {
                real = batchInfo.TopLeft.Real;

                for (int x = batchInfo.ScreenArea.Left; x < batchInfo.ScreenArea.Right; x++)
                {
                    Result[x, y].Point = new Complex(real, imaginary);
                    int iterations = 0;
                    Result[x, y].IsInMandelbrot = IsInMandelbrot(Result[x, y].Point, batchInfo.MaxIterations, out iterations);
                    Result[x, y].Iterations = iterations;
                    Result[x, y].Colour = Colourizer.GetColour(iterations, batchInfo.MaxIterations);

                    OnProgress();

                    // Move to the next value horizontally.
                    real += _xStep;
                }
                // Move to the next value vertically.
                imaginary -= _yStep;
            }
        }

        /// <summary>
        /// This method determines if a given complex number is in the Mandelbrot set.
        /// It also determines how many iterations were used before the result was determined.
        /// </summary>
        /// <param name="c">The point we are checking</param>
        /// <param name="maxIterations">The maximum number of iterations to try</param>
        /// <param name="iterations">(ref) The number of iterations taken</param>
        /// <returns>True if the number is in the set, false otherwise</returns>
        private bool IsInMandelbrot(Complex c, int maxIterations, out int iterations)
        {
            iterations = 0;
            Complex z = new Complex(0, 0);

            while (iterations < maxIterations)
            {
                iterations++;
                z = (z * z) + c;

                if (Complex.Abs(z) > _limit)
                {
                    return false;
                }
            }

            return true;
        }

        private void OnProgress()
        {
            if (this.Progress != null)
            {
                this.Progress(this, EventArgs.Empty);
            }
        }

    }
}