﻿using MandelbrotGenerator.Model;
using System.Drawing;

namespace MandelbrotGenerator.BusinessLogic
{
    public class Colourizer
    {
        public static Color GetColour(int iterations, int maxIterations)
        {
            return GetStandardColour(iterations, maxIterations);
        }

        public static Color GetStandardColour(int v, int maxIterations)
        {
            if (v == maxIterations)
                return Color.Black;
            else
                return Color.FromArgb(255 - (v * 3 % 256), 255 - (v * 7 % 256), 255 - (v * 13 % 256));
        }

        private static Color GetHSVColour(int iterations, int maxIterations)
        {
            var h = (iterations == maxIterations) ? 0 : iterations % 360;
            var s = (iterations == maxIterations) ? 0 : 100;
            var v = (iterations == maxIterations) ? 0 : 100;
            HSV hsv = new HSV(h, s, v);
            return hsv.ToRGB();
        }

        private static Color GetGreyColour(int iterations, int maxIterations)
        {
            // Grey Logic
            int strength = 255 - (int)(((double)iterations / (double)maxIterations) * 255);
            return Color.FromArgb(strength, strength, strength);
        }

    }
}
