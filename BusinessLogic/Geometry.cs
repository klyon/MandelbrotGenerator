﻿using MandelbrotGenerator.Model;
using System.Drawing;
using System.Numerics;

namespace MandelbrotGenerator.BusinessLogic
{
    public class Geometry
    {
        public static ComplexRectangle GetComplexArea(GeneratorSettings generatorSettings)
        {
            var centre = generatorSettings.Centre;
            var resolution = generatorSettings.Resolution;
            var zoom = generatorSettings.Zoom;

            if (zoom < 1)
            {
                zoom = 1;
            }

            SizeF size = new SizeF(Generator.DefaultBounds.Width / zoom, Generator.DefaultBounds.Height / zoom);
            var stepSize = GetStepSize(resolution, size);

            var minReal = centre.Real - (((resolution.Width / 2) - 1) * stepSize);
            var maxImag = centre.Imaginary + (((resolution.Height / 2) - 1) * stepSize);
            var width = (resolution.Width - 1) * stepSize;
            var height = (resolution.Height - 1) * stepSize;
            var area = new ComplexRectangle(minReal, maxImag, width, height);
            return area;
        }

        /// <summary>
        /// The step size is the amount we move in the complex plane from one pixel to the next.
        /// This is determined based on a number of factors:
        /// 1) The resolution of the output image gives us the width and height of pixels needed.
        /// 2) The selected complex area is the area of the graph to be rendered.
        /// 
        /// If the aspect ratio of these is different, we need to either match the width or height.
        /// The rule is that the entirety of the complexSize must be visible, so we add in the
        /// dimension that does not fit.
        /// </summary>
        /// <param name="imageResolution">The resolution of the output image to be rendered.</param>
        /// <param name="complexSize">The size of the selected area of the complex plane</param>
        /// <returns>The amount of complex delta per pixel</returns>
        private static double GetStepSize(Size imageResolution, SizeF complexSize)
        {
            var complexSizeAspectRatio = complexSize.Width / complexSize.Height;
            var resolutionAspectRatio = imageResolution.Width / (double)imageResolution.Height;

            var stepSize = 0.0;

            if (resolutionAspectRatio < complexSizeAspectRatio)
            {
                // Match width
                stepSize = complexSize.Width / imageResolution.Width;
            }
            else
            {
                // Match height
                stepSize = complexSize.Height / imageResolution.Height;
            }
            return stepSize;
        }
    }
}