﻿using System;
using System.Drawing;
using System.Numerics;

namespace MandelbrotGenerator.Model
{
    public class BatchInfo
    {
        public Rectangle ScreenArea;

        public Complex TopLeft { get; set; }

        /// <summary>
        /// This is shared per-batch to avoid the need for a private variable. All batches should use the same value, though.
        /// </summary>
        public int MaxIterations { get; set; }

        public override string ToString()
        {
            return String.Format("ScreenArea={0}, TopLeft={1}", ScreenArea, TopLeft);
        }
    }
}
