﻿using System.Drawing;
using System.Numerics;

namespace MandelbrotGenerator.Model
{
    public class GeneratorSettings
    {
        public GeneratorSettings()
        {
            Centre = new Complex(-0.5, 0);
            Zoom = 1;
            Resolution = new Size(300, 300);
            MaxIterations = 360;
        }

        /// <summary>
        /// The area of the complex plane included in this rendering.
        /// </summary>
        public ComplexRectangle Area { get; set; }
        /// <summary>
        /// The co-ordinates of the centre of the image on the complex plane.
        /// </summary>
        public Complex Centre { get; set; }
        /// <summary>
        /// The level of zoom that was applied to the original area of the complex plane.
        /// TODO: Is this property redundant? Can't we derive this by comparing Area to the default area?
        /// </summary>
        public int Zoom { get; set; }
        /// <summary>
        /// The resolution (in pixels) of the output image.
        /// </summary>
        public Size Resolution { get; set; }
        /// <summary>
        /// The maximum number of iterations to test a pixel for,
        /// before assuming it's in the set.
        /// </summary>
        public int MaxIterations { get; set; }
    }
}
