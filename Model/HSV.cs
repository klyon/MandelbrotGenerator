﻿using System;
using System.Drawing;

namespace MandelbrotGenerator.Model
{
    public class HSV
    {
        private int _hue;
        private int _saturation;
        private int _value;

        public HSV()
        {

        }

        public HSV(int hue, int saturation, int value)
        {
            Hue = hue;
            Saturation = saturation;
            Value = value;
        }

        public int Hue
        {
            get
            {
                return _hue;
            }
            set
            {
                if (value < 0 || value > 360)
                {
                    throw new ArgumentException("Value must be between 0 and 360.", "Hue");
                }
                _hue = value;
            }
        }
        public int Saturation
        {
            get
            {
                return _saturation;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentException("Value must be between 0 and 100.", "Saturation");
                }
                _saturation = value;
            }
        }
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentException("Value must be between 0 and 100.", "Value");
                }
                _value = value;
            }
        }

        /// <summary>
        /// Formula from here: http://en.wikipedia.org/wiki/HSL_color_space#From_HSV
        /// 
        /// Alternatives:
        /// 
        /// http://www.alvyray.com/Papers/hsv2rgb.htm
        /// http://snipplr.com/view/14590/hsv-to-rgb/
        /// </summary>
        /// <returns></returns>
        public Color ToRGB()
        {
            var s = (Saturation / (double)100);
            var v = (Value / (double)100);

            var c = s * v;
            var hDash = (double)Hue / 60;

            var i = (int)Math.Floor(hDash);
            var f = (hDash - i);

            var p = v * (1 - s);
            var q = v * (1 - s * f);
            var t = v * (1 - s * (1 - f));

            double r = 0;
            double g = 0;
            double b = 0;

            switch (i)
            {
                case 0:
                    r = v;
                    g = t;
                    b = p;
                    break;
                case 1:
                    r = q;
                    g = v;
                    b = p;
                    break;
                case 2:
                    r = p;
                    g = v;
                    b = t;
                    break;
                case 3:
                    r = p;
                    g = q;
                    b = v;
                    break;
                case 4:
                    r = t;
                    g = p;
                    b = v;
                    break;
                default: // case 5:
                    r = v;
                    g = p;
                    b = q;
                    break;
            }


            return Color.FromArgb(
                (int)Math.Floor(Math.Round(r * 255)),
                (int)Math.Floor(Math.Round(g * 255)),
                (int)Math.Floor(Math.Round(b * 255))
                );
        }
    }
}