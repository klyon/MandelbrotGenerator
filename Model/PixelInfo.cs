﻿using System.Drawing;
using System.Numerics;

namespace MandelbrotGenerator.Model
{
    /// <summary>
    /// Info for a single pixel in the complex plane.
    /// </summary>
    public struct PixelInfo
    {
        /// <summary>
        /// The corresponding point on the complex plane.
        /// </summary>
        public Complex Point { get; set; }
        /// <summary>
        /// True if the point is in the mandelbrot set, false otherwise.
        /// </summary>
        public bool IsInMandelbrot { get; set; }
        /// <summary>
        /// The number of iterations this point went through the Mandelbrot formula before exiting.
        /// </summary>
        public int Iterations { get; set; }
        /// <summary>
        /// The colour of the pixel.
        /// </summary>
        public Color Colour { get; set; }
    }
}
