﻿using System.Numerics;

namespace MandelbrotGenerator.Model
{
    public class ComplexRectangle
    {
        private Complex _topLeft;
        private double _width;
        private double _height;

        public ComplexRectangle(double minReal, double maxImag, double width, double height)
        {
            _topLeft = new Complex(minReal, maxImag);
            _width = width;
            _height = height;
        }

        public double Left
        {
            get
            {
                return _topLeft.Real;
            }
        }

        public double Right
        {
            get
            {
                return _topLeft.Real + _width;
            }
        }

        public double Top
        {
            get
            {
                return _topLeft.Imaginary;
            }
        }

        public double Bottom
        {
            get
            {
                return _topLeft.Imaginary - _height;
            }
        }

        public double Width
        {
            get
            {
                return _width;
            }
        }

        public double Height
        {
            get
            {
                return _height;
            }
        }

    }
}
