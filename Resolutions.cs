﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MandelbrotGenerator
{
    static class Resolutions
    {
        private static List<KeyValuePair<string, Size>> _options = null;

        public static List<KeyValuePair<string, Size>> GetOptions()
        {
            if (_options == null)
            {
                _options = new List<KeyValuePair<string, Size>>();
                // 1:1
                AddSize(300, 300);
                AddSize(800, 800);
                AddSize(4140, 4140); // 11.5" x 11.5" @ 360 dpi
                // 3:2
                AddSize(300, 200);
                AddSize(450, 300);
                AddSize(600, 400);
                AddSize(750, 500);
                AddSize(900, 600);
                AddSize(1050, 700);
                AddSize(1200, 800);
                AddSize(1350, 900);
                AddSize(1500, 1000);
                AddSize(3300, 2550);
                // 4:3
                AddSize(320, 240);
                AddSize(800, 600);
                AddSize(1024, 768);
                AddSize(1152, 864);
                AddSize(1280, 960);
                // 16:9
                AddSize(1280, 720);
                AddSize(1600, 900);
                // 8:5
                AddSize(1280, 800);
                AddSize(1680, 1050);
                // Others
                AddSize(1280, 768);
                AddSize(1280, 1024);
                AddSize(1360, 768);
                AddSize(1366, 768);
                AddSize(1600, 1024);
            }

            return _options;
        }

        private static void AddSize(int width, int height)
        {
            Size size = new Size(width, height);
            _options.Add(
                new KeyValuePair<string, Size>(
                    String.Format("{0} x {1} ({2})", size.Width, size.Height, size.GetAspectRatio()),
                    size));
        }

    }
}