﻿using MandelbrotGenerator.BusinessLogic;
using MandelbrotGenerator.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MandelbrotGenerator.UI
{
    public partial class BrowserForm : Form
    {
        private Pen _rectPen = new Pen(Color.Gold);

        private bool _showRect = false;

        private int _selectedZoomFactor = 1;

        /// <summary>
        /// The minimum possible value for zoom.
        /// </summary>
        private int _minZoom = 1;

        /// <summary>
        /// The maximum possible value for zoom.
        /// </summary>
        private int _maxZoom = 10;

        /// <summary>
        /// The centre of the selection rectangle for the next image.
        /// </summary>
        private Point _rectCentre;

        /// <summary>
        /// The area of the selection rectangle for the next image.
        /// </summary>
        private Rectangle _rect;

        private Size _selectedResolution;

        public PixelInfo[,] Data { get; private set; }

        public BrowserForm()
        {
            InitializeComponent();
            _selectedResolution = new Size(300, 300);
            this.outputPictureBox.Size = _selectedResolution;
        }

        private async void goButton_Click(object sender, EventArgs e)
        {
            Complex? complexCentre = ParseCentre();

            if (!complexCentre.HasValue)
            {
                MessageBox.Show(this, "You must enter the centre in the form \"(x, y)\"", "Invalid Co-Ordinates", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int? zoom = ParseZoom();

            if (!zoom.HasValue)
            {
                MessageBox.Show(this, "Zoom must be an integer value.", "Invalid Zoom", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _selectedZoomFactor = zoom.Value;

            await GenerateNewImage(complexCentre.Value);
        }

        private Complex? ParseCentre()
        {
            string[] parts = this.centreTextBox.Text.Replace("(", String.Empty).Replace(")", String.Empty).Split(',');
            Complex? centre = null;
            double real;
            double imaginary;
            bool validReal = Double.TryParse(parts.Length > 0 ? parts[0] : String.Empty, out real);
            bool validImag = Double.TryParse(parts.Length > 1 ? parts[1] : String.Empty, out imaginary);
            if (validReal && validImag)
            {
                centre = new Complex(real, imaginary);
            }
            return centre;
        }

        private int? ParseZoom()
        {
            int val;
            if (int.TryParse(this.zoomTextBox.Text, out val))
            {
                return val;
            }
            else
            {
                return null;
            }
        }

        private async Task GenerateNewImage(Complex complexCentre)
        {
            var generatorSettings = new GeneratorSettings();
            generatorSettings.Centre = complexCentre;
            generatorSettings.Zoom = _selectedZoomFactor;
            generatorSettings.Area = Geometry.GetComplexArea(generatorSettings);
            var generator = new Generator();
            await generator.GenerateSet(generatorSettings, false);
            await LoadData(generatorSettings, generator.Result, false);
        }

        public async Task LoadData(GeneratorSettings generatorSettings, PixelInfo[,] resultData, bool async = true)
        {
            Data = resultData;
            //_selectedResolution = generatorSettings.Resolution;
            //MaxIterations = generatorSettings.MaxIterations;
            //CurrentImageZoom = generatorSettings.Zoom;

            //var item = this.resolutionToolStripMenuItem.DropDownItems.OfType<ToolStripMenuItem>().Where(mi => (Size)mi.Tag == generatorSettings.Resolution).FirstOrDefault();

            //if (item != null)
            //{
            //    item.Checked = true;
            //}

            //this.Text = String.Format("{0} x{1} @{2}x{3}", generatorSettings.Centre, generatorSettings.Zoom, _selectedResolution.Width, _selectedResolution.Height);
            this.outputPictureBox.Size = _selectedResolution;
            //this.ClientSize = new Size(_selectedResolution.Width, _selectedResolution.Height + this.statusStrip.Height);
            this.CenterToParent();

            if (async)
                await Task.Run(() => DrawBitmap());
            else
                DrawBitmap();

            //// Default rectangle centre and size based on image resolution.
            //_rect = new Rectangle(new Point(0, 0), _selectedResolution);
            //_rectCentre = new Point((_selectedResolution.Width / 2) - 1, (_selectedResolution.Height / 2) - 1);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            if (e.KeyChar == '+' || e.KeyChar == '=')
            {
                ZoomRect(1, _rectCentre);
            }
            else if (e.KeyChar == '-' || e.KeyChar == '_')
            {
                ZoomRect(-1, _rectCentre);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.Escape)
            {
                _showRect = false;
                UpdateRectangle();
                UpdateStatusText();
                return;
            }

            int dx = 0;
            int dy = 0;

            if (e.KeyCode == Keys.Left)
            {
                dx = -1;
            }
            else if (e.KeyCode == Keys.Right)
            {
                dx = 1;
            }
            else if (e.KeyCode == Keys.Up)
            {
                dy = -1;
            }
            else if (e.KeyCode == Keys.Down)
            {
                dy = 1;
            }

            if (dx != 0 || dy != 0)
            {
                MoveRect(dx, dy);
            }
        }

        private void DrawBitmap()
        {
            Bitmap _bitmap = new Bitmap(Data.GetLength(0), Data.GetLength(1));

            for (int x = 0; x < Data.GetLength(0); x++)
            {
                for (int y = 0; y < Data.GetLength(1); y++)
                {
                    _bitmap.SetPixel(x, y, Data[x, y].Colour);
                }
            }

            this.outputPictureBox.Image = _bitmap;
        }

        private void outputPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _showRect = true;

                if (_showRect)
                {
                    _rectCentre = e.Location;
                    UpdateRectangle();
                    //UpdateStatusText();
                }
            }
        }

        /// <summary>
        /// This updates the dimensions of the zoom rectangle.
        /// </summary>
        private void UpdateRectangle()
        {
            if (_showRect)
            {
                var newSize = Util.GetRectagleSize(outputPictureBox.Size, _selectedResolution, _selectedZoomFactor);

                int x1 = (_rectCentre.X - (newSize.Width / 2));
                int x2 = (_rectCentre.X + (newSize.Width / 2));
                int y1 = (_rectCentre.Y - (newSize.Height / 2));
                int y2 = (_rectCentre.Y + (newSize.Height / 2));

                _rect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }

            this.outputPictureBox.Invalidate();
        }

        private void MoveRect(int dx, int dy)
        {
            _rectCentre.X += dx;
            _rectCentre.Y += dy;

            _rectCentre.X = (_rectCentre.X < 0) ? 0 : _rectCentre.X;
            _rectCentre.Y = (_rectCentre.Y < 0) ? 0 : _rectCentre.Y;
            _rectCentre.X = (_rectCentre.X >= _selectedResolution.Width) ? (_selectedResolution.Width - 1) : _rectCentre.X;
            _rectCentre.Y = (_rectCentre.Y >= _selectedResolution.Height) ? (_selectedResolution.Height - 1) : _rectCentre.Y;

            _showRect = true;

            UpdateRectangle();
            UpdateStatusText();
        }

        private void UpdateStatusText()
        {
            //throw new NotImplementedException();
        }

        private void ZoomRect(int amount, Point centre)
        {
            _selectedZoomFactor = _selectedZoomFactor + amount;
            _selectedZoomFactor = (_selectedZoomFactor < _minZoom) ? _minZoom : _selectedZoomFactor; // Ensure not zoomed out beyond minimum.
            _selectedZoomFactor = (_selectedZoomFactor > _maxZoom) ? _maxZoom : _selectedZoomFactor; // Ensure not zoomed in beyond maximum.
            _rectCentre = centre;
            _showRect = true;

            UpdateRectangle();
            UpdateStatusText();
        }

        private void outputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (_showRect)
            {
                if (e.ClipRectangle.Contains(_rect) || e.ClipRectangle.IntersectsWith(_rect))
                {
                    e.Graphics.DrawRectangle(_rectPen, _rect);
                }
            }
        }

    }
}
