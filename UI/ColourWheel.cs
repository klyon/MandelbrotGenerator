﻿using MandelbrotGenerator.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MandelbrotGenerator.UI
{
    public partial class ColourWheel : UserControl
    {
        private Bitmap _bitmap = null;

        public ColourWheel()
        {
            InitializeComponent();
            _bitmap = new Bitmap(this.Width, this.Height);
            this.pictureBox1.Image = _bitmap;
        }

        protected override void OnResize(EventArgs e)
        {
            if (_bitmap != null)
                _bitmap.Dispose();
            _bitmap = new Bitmap(this.Width, this.Height);
            this.pictureBox1.Image = _bitmap;

            base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var centre = new Point(this.Width / 2, this.Height / 2);
            var radius = Math.Min(this.Width, this.Height) / 2;

            var maxX = e.ClipRectangle.Width == 0 ? this.Width - 1 : e.ClipRectangle.Right;
            var maxY = e.ClipRectangle.Height == 0 ? this.Height - 1 : e.ClipRectangle.Bottom;

            for (int x = e.ClipRectangle.Left; x <= maxX; x++)
            {
                for (int y = e.ClipRectangle.Top; y <= maxY; y++)
                {
                    var hue = CalculateHue(x, y, centre, radius);
                    var saturation = CalculateSaturation(x, y, centre, radius);

                    if (!hue.HasValue || !saturation.HasValue)
                    {
                        // No value - must not be in circle.
                        continue;
                    }
                    HSV hsv = new HSV((int)hue, (int)(saturation * 100), 100);
                    _bitmap.SetPixel(x, y, hsv.ToRGB());
                }
            }
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            OnMouseEnter(e);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            OnMouseLeave(e);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            OnMouseMove(e);
        }

        private double? CalculateHue(int x, int y, Point centre, int radius)
        {
            var xDiff = x - centre.X;
            var yDiff = y - centre.Y;

            // Find the length of the line from the centre to this point
            var length = Math.Sqrt((xDiff * xDiff) + (yDiff * yDiff));

            if (length > radius)
            {
                // This point is not in the circle.
                return null;
            }

            double angle = 0;

            if (xDiff == 0)
            {
                angle = (yDiff >= 0) ? 90 : 270;
            }
            else if (yDiff == 0)
            {
                angle = (xDiff >= 0) ? 0 : 180;
            }
            else
            {
                angle = Math.Atan((double)Math.Abs(yDiff) / (double)Math.Abs(xDiff)) * (180 / Math.PI);
            }

            if (xDiff > 0 && yDiff > 0)
            {
                // Do Nothing
            }
            else if (xDiff < 0 && yDiff > 0)
            {
                angle = 180 - angle;
            }
            else if (xDiff < 0 && yDiff < 0)
            {
                angle += 180;
            }
            else if (xDiff > 0 && yDiff < 0)
            {
                angle = 360 - angle;
            }

            return angle;
        }

        private double? CalculateSaturation(int x, int y, Point centre, int radius)
        {
            var xDiff = x - centre.X;
            var yDiff = y - centre.Y;

            // Find the length of the line from the centre to this point
            var length = Math.Sqrt((xDiff * xDiff) + (yDiff * yDiff));

            if (length > radius)
            {
                // This point is not in the circle.
                return null;
            }

            return (length / radius);
        }

    }
}
