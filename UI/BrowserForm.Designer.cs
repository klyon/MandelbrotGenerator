﻿namespace MandelbrotGenerator.UI
{
    partial class BrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navigationPanel = new System.Windows.Forms.Panel();
            this.goButton = new System.Windows.Forms.Button();
            this.zoomTextBox = new System.Windows.Forms.TextBox();
            this.zoomLabel = new System.Windows.Forms.Label();
            this.centreTextBox = new System.Windows.Forms.TextBox();
            this.centreLabel = new System.Windows.Forms.Label();
            this.outputPictureBox = new System.Windows.Forms.PictureBox();
            this.navigationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outputPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // navigationPanel
            // 
            this.navigationPanel.AutoSize = true;
            this.navigationPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.navigationPanel.Controls.Add(this.goButton);
            this.navigationPanel.Controls.Add(this.zoomTextBox);
            this.navigationPanel.Controls.Add(this.zoomLabel);
            this.navigationPanel.Controls.Add(this.centreTextBox);
            this.navigationPanel.Controls.Add(this.centreLabel);
            this.navigationPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.navigationPanel.Location = new System.Drawing.Point(0, 0);
            this.navigationPanel.Name = "navigationPanel";
            this.navigationPanel.Size = new System.Drawing.Size(784, 29);
            this.navigationPanel.TabIndex = 0;
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(333, 3);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(75, 23);
            this.goButton.TabIndex = 4;
            this.goButton.Text = "&Go";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // zoomTextBox
            // 
            this.zoomTextBox.Location = new System.Drawing.Point(227, 5);
            this.zoomTextBox.Name = "zoomTextBox";
            this.zoomTextBox.Size = new System.Drawing.Size(100, 20);
            this.zoomTextBox.TabIndex = 3;
            this.zoomTextBox.Text = "1";
            // 
            // zoomLabel
            // 
            this.zoomLabel.AutoSize = true;
            this.zoomLabel.Location = new System.Drawing.Point(187, 8);
            this.zoomLabel.Name = "zoomLabel";
            this.zoomLabel.Size = new System.Drawing.Size(34, 13);
            this.zoomLabel.TabIndex = 2;
            this.zoomLabel.Text = "&Zoom";
            // 
            // centreTextBox
            // 
            this.centreTextBox.Location = new System.Drawing.Point(47, 5);
            this.centreTextBox.Name = "centreTextBox";
            this.centreTextBox.Size = new System.Drawing.Size(134, 20);
            this.centreTextBox.TabIndex = 1;
            this.centreTextBox.Text = "(-0.5, 0)";
            // 
            // centreLabel
            // 
            this.centreLabel.AutoSize = true;
            this.centreLabel.Location = new System.Drawing.Point(3, 8);
            this.centreLabel.Name = "centreLabel";
            this.centreLabel.Size = new System.Drawing.Size(38, 13);
            this.centreLabel.TabIndex = 0;
            this.centreLabel.Text = "&Centre";
            // 
            // outputPictureBox
            // 
            this.outputPictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.outputPictureBox.Location = new System.Drawing.Point(0, 29);
            this.outputPictureBox.Name = "outputPictureBox";
            this.outputPictureBox.Size = new System.Drawing.Size(500, 500);
            this.outputPictureBox.TabIndex = 1;
            this.outputPictureBox.TabStop = false;
            this.outputPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.outputPictureBox_Paint);
            this.outputPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.outputPictureBox_MouseDown);
            // 
            // BrowserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.outputPictureBox);
            this.Controls.Add(this.navigationPanel);
            this.Name = "BrowserForm";
            this.Text = "BrowserForm";
            this.navigationPanel.ResumeLayout(false);
            this.navigationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outputPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel navigationPanel;
        private System.Windows.Forms.TextBox zoomTextBox;
        private System.Windows.Forms.Label zoomLabel;
        private System.Windows.Forms.TextBox centreTextBox;
        private System.Windows.Forms.Label centreLabel;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.PictureBox outputPictureBox;
    }
}