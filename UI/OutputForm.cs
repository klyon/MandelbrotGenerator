﻿using MandelbrotGenerator.BusinessLogic;
using MandelbrotGenerator.Model;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MandelbrotGenerator.UI
{
    public partial class OutputForm : Form
    {
        private Pen _rectPen = new Pen(Color.Gold);

        private bool _showRect = false;

        /// <summary>
        /// The resolution selected for the next image.
        /// </summary>
        private Size _selectedResolution;

        /// <summary>
        /// The minimum possible value for zoom.
        /// </summary>
        private int _minZoom = 1;

        /// <summary>
        /// The maximum possible value for zoom.
        /// </summary>
        private int _maxZoom = 10;

        /// <summary>
        /// The relative zoom, from the current image.
        /// To get the actual zoom of a new image, this is multiplied by CurrentImageZoom.
        /// </summary>
        private int _selectedZoomFactor = 1;

        /// <summary>
        /// The centre of the selection rectangle for the next image.
        /// </summary>
        private Point _rectCentre;

        /// <summary>
        /// The area of the selection rectangle for the next image.
        /// </summary>
        private Rectangle _rect;

        public OutputForm()
        {
            InitializeComponent();

            foreach (var item in Resolutions.GetOptions())
            {
                var newItem = new ToolStripMenuItem(item.Key);
                newItem.Tag = item.Value;
                newItem.Click += new EventHandler(Resolution_Click);
                this.resolutionToolStripMenuItem.DropDownItems.Add(newItem);
            }

            this.MouseWheel += new MouseEventHandler(OutputForm_MouseWheel);
        }

        public PixelInfo[,] Data { get; private set; }

        public int MaxIterations { get; private set; }

        /// <summary>
        /// The level of zoom that was used to generate the displayed image.
        /// </summary>
        public int CurrentImageZoom { get; private set; }

        public async Task LoadData(GeneratorSettings generatorSettings, PixelInfo[,] resultData, bool async = true)
        {
            Data = resultData;
            _selectedResolution = generatorSettings.Resolution;
            MaxIterations = generatorSettings.MaxIterations;
            CurrentImageZoom = generatorSettings.Zoom;

            var item = this.resolutionToolStripMenuItem.DropDownItems.OfType<ToolStripMenuItem>().Where(mi => (Size)mi.Tag == generatorSettings.Resolution).FirstOrDefault();

            if (item != null)
            {
                item.Checked = true;
            }

            this.Text = String.Format("{0} x{1} @{2}x{3}", generatorSettings.Centre, generatorSettings.Zoom, _selectedResolution.Width, _selectedResolution.Height);
            this.outputPictureBox.Size = _selectedResolution;
            this.ClientSize = new Size(_selectedResolution.Width, _selectedResolution.Height + this.statusStrip.Height);
            this.CenterToParent();

            if (async)
                await Task.Run(() => DrawBitmap());
            else
                DrawBitmap();

            // Default rectangle centre and size based on image resolution.
            _rect = new Rectangle(new Point(0, 0), _selectedResolution);
            _rectCentre = new Point((_selectedResolution.Width / 2) - 1, (_selectedResolution.Height / 2) - 1);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            if (e.KeyChar == '+' || e.KeyChar == '=')
            {
                ZoomRect(1, _rectCentre);
            }
            else if (e.KeyChar == '-' || e.KeyChar == '_')
            {
                ZoomRect(-1, _rectCentre);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.Escape)
            {
                _showRect = false;
                UpdateRectangle();
                UpdateStatusText();
                return;
            }

            int dx = 0;
            int dy = 0;

            if (e.KeyCode == Keys.Left)
            {
                dx = -1;
            }
            else if (e.KeyCode == Keys.Right)
            {
                dx = 1;
            }
            else if (e.KeyCode == Keys.Up)
            {
                dy = -1;
            }
            else if (e.KeyCode == Keys.Down)
            {
                dy = 1;
            }

            if (dx != 0 || dy != 0)
            {
                MoveRect(dx, dy);
            }
        }

        private void outputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (_showRect)
            {
                if (e.ClipRectangle.Contains(_rect) || e.ClipRectangle.IntersectsWith(_rect))
                {
                    e.Graphics.DrawRectangle(_rectPen, _rect);
                }
            }
        }

        private void OutputForm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (this.outputPictureBox.ClientRectangle.Contains(e.Location))
            {
                if (e.Delta > 0)
                {
                    ZoomRect(1, e.Location);
                }
                else
                {
                    ZoomRect(-1, e.Location);
                }
            }
        }

        private void outputPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _showRect = true;

                if (_showRect)
                {
                    _rectCentre = e.Location;
                    UpdateRectangle();
                    UpdateStatusText();
                }
            }
        }

        private void outputPictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _showRect = false;

                UpdateRectangle();
            }
        }

        private void saveToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            SaveImage();
        }

        private async void zoomToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            await GenerateNewImage();
        }

        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveImage();
        }

        private async void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await GenerateNewImage();
        }

        private void Resolution_Click(object sender, EventArgs e)
        {
            SelectNewResolution(sender as ToolStripMenuItem);
        }

        private void Generator_Progress(object sender, EventArgs e)
        {
            Action progressAction = () => IncrementProgress();
            this.BeginInvoke(progressAction);
        }

        private void DrawBitmap()
        {
            Bitmap _bitmap = new Bitmap(Data.GetLength(0), Data.GetLength(1));

            for (int x = 0; x < Data.GetLength(0); x++)
            {
                for (int y = 0; y < Data.GetLength(1); y++)
                {
                    _bitmap.SetPixel(x, y, Data[x, y].Colour);
                }
            }

            this.outputPictureBox.Image = _bitmap;
        }

        private void UpdateStatusText()
        {
            String statusText = String.Empty;

            if (_showRect)
            {
                statusText = String.Format("{0} x{1}", Data[_rectCentre.X, _rectCentre.Y].Point, (_selectedZoomFactor * CurrentImageZoom));
            }
            else
            {
                statusText = "Ready";
            }

            this.toolStripStatusLabel.Text = statusText;
        }

        /// <summary>
        /// This updates the dimensions of the zoom rectangle.
        /// </summary>
        private void UpdateRectangle()
        {
            if (_showRect)
            {
                var newSize = Util.GetRectagleSize(outputPictureBox.Size, _selectedResolution, _selectedZoomFactor);

                int x1 = (_rectCentre.X - (newSize.Width / 2));
                int x2 = (_rectCentre.X + (newSize.Width / 2));
                int y1 = (_rectCentre.Y - (newSize.Height / 2));
                int y2 = (_rectCentre.Y + (newSize.Height / 2));

                _rect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }

            this.outputPictureBox.Invalidate();
        }

        private void SaveImage()
        {
            saveFileDialog.FileName = String.Format("{0}.png", this.Text);
            var dialogResult = saveFileDialog.ShowDialog(this);

            if (dialogResult == DialogResult.OK)
            {
                //var result = ImageCodecInfo.GetImageEncoders();

                var fileName = saveFileDialog.FileName;
                var imageFormat = ImageFormat.Png;
                (this.outputPictureBox.Image as Bitmap).Save(fileName, imageFormat);

                //fileName = fileName.Replace(".png", ".bmp");
                //imageFormat = ImageFormat.Bmp;
                //(this.outputPictureBox.Image as Bitmap).Save(fileName, imageFormat);
            }
        }

        private void IncrementProgress()
        {
            this.toolStripProgressBar1.Increment(1);
        }

        private async Task ProcessResults(GeneratorSettings generatorSettings, PixelInfo[,] resultData)
        {
            OutputForm form = new OutputForm();
            await form.LoadData(generatorSettings, resultData);
            form.Show();

            this.toolStripProgressBar1.Visible = false;

            this.saveToolStripSplitButton.Visible = true;
            this.zoomToolStripSplitButton.Visible = true;

            UpdateStatusText();
        }

        private void MoveRect(int dx, int dy)
        {
            _rectCentre.X += dx;
            _rectCentre.Y += dy;

            _rectCentre.X = (_rectCentre.X < 0) ? 0 : _rectCentre.X;
            _rectCentre.Y = (_rectCentre.Y < 0) ? 0 : _rectCentre.Y;
            _rectCentre.X = (_rectCentre.X >= _selectedResolution.Width) ? (_selectedResolution.Width - 1) : _rectCentre.X;
            _rectCentre.Y = (_rectCentre.Y >= _selectedResolution.Height) ? (_selectedResolution.Height - 1) : _rectCentre.Y;

            _showRect = true;

            UpdateRectangle();
            UpdateStatusText();
        }

        private void ZoomRect(int amount, Point centre)
        {
            _selectedZoomFactor = _selectedZoomFactor + amount;
            _selectedZoomFactor = (_selectedZoomFactor < _minZoom) ? _minZoom : _selectedZoomFactor; // Ensure not zoomed out beyond minimum.
            _selectedZoomFactor = (_selectedZoomFactor > _maxZoom) ? _maxZoom : _selectedZoomFactor; // Ensure not zoomed in beyond maximum.
            _rectCentre = centre;
            _showRect = true;

            UpdateRectangle();
            UpdateStatusText();
        }

        private void SelectNewResolution(ToolStripMenuItem selectedItem)
        {
            foreach (var item in this.resolutionToolStripMenuItem.DropDownItems)
            {
                var menuItem = (item as ToolStripMenuItem);

                if (menuItem == selectedItem)
                {
                    menuItem.Checked = true;
                    _selectedResolution = (Size)menuItem.Tag;
                }
                else
                {
                    menuItem.Checked = false;
                }
            }

            UpdateRectangle();
        }

        private async Task GenerateNewImage()
        {
            //_generator.Progress += new EventHandler<EventArgs>(Generator_Progress);

            // Determine complex area based on number of pixels in selected rect
            var complexCentre = Data[_rectCentre.X, _rectCentre.Y];

            double delta = GetStepSize(ref complexCentre);

            var width = (_rect.Width - 1) * delta;
            var height = (_rect.Height - 1) * delta;
            var minReal = complexCentre.Point.Real - (width / 2);
            var maxImag = complexCentre.Point.Imaginary + (height / 2);

            var rect = new ComplexRectangle(minReal, maxImag, width, height);

            var generatorSettings = new GeneratorSettings();
            generatorSettings.Centre = complexCentre.Point;
            generatorSettings.Area = rect;
            generatorSettings.Zoom = this.CurrentImageZoom * _selectedZoomFactor;
            generatorSettings.Resolution = _selectedResolution;
            generatorSettings.MaxIterations = this.MaxIterations;

            //this.toolStripProgressBar1.Maximum = (_selectedResolution.Width * _selectedResolution.Height);
            //this.toolStripProgressBar1.Value = 0;
            //this.toolStripProgressBar1.Visible = true;

            this.saveToolStripSplitButton.Visible = false;
            this.zoomToolStripSplitButton.Visible = false;
            this.toolStripStatusLabel.Text = "Generating image";

            var generator = new Generator();
            await generator.GenerateSet(generatorSettings);
            await ProcessResults(generatorSettings, generator.Result);
        }

        private double GetStepSize(ref PixelInfo complexCentre)
        {
            double delta = complexCentre.Point.Real - Data[_rectCentre.X - 1, _rectCentre.Y].Point.Real;
            return delta;
        }

    }
}
