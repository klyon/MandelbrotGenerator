﻿using MandelbrotGenerator.BusinessLogic;
using MandelbrotGenerator.Model;
using System;
using System.Drawing;
using System.Numerics;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MandelbrotGenerator.UI
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();

            this.resolutionComboBox.DataSource = Resolutions.GetOptions();

            //_generator.Progress += new EventHandler<EventArgs>(Generator_Progress);
        }

        private async void generateButton_Click(object sender, System.EventArgs e)
        {
            await GenerateNewImage();
        }

        private async Task GenerateNewImage()
        {
            var centre = ParseCentre(this.centreTextBox.Text);
            var resolution = (Size)this.resolutionComboBox.SelectedValue;
            int maxIterations = Int32.Parse(this.maxIterationsTextBox.Text);
            int zoom = Int32.Parse(this.zoomTextBox.Text);

            // Ensure zoom is no smaller than 1.
            zoom = (zoom < 1) ? 1 : zoom;

            var area = GetDefaultArea(ref centre, ref resolution, zoom);

            var generatorSettings = new GeneratorSettings();
            generatorSettings.Area = area;
            generatorSettings.Centre = centre;
            generatorSettings.Zoom = zoom;
            generatorSettings.Resolution = resolution;
            generatorSettings.MaxIterations = maxIterations;

            //this.progressBar1.Maximum = (resolution.Width * resolution.Height);
            //this.progressBar1.Value = 0;
            //this.progressBar1.Visible = true;

            var generator = new Generator();
            await generator.GenerateSet(generatorSettings);
            await ProcessResults(generatorSettings, generator.Result);
        }

        public static ComplexRectangle GetDefaultArea(ref Complex centre, ref Size resolution, int zoom)
        {
            var stepSize = GetStepSize(resolution, Generator.DefaultBounds);

            var minReal = centre.Real - (((resolution.Width / 2) - 1) * stepSize);
            var maxImag = centre.Imaginary + (((resolution.Height / 2) - 1) * stepSize);
            var width = (resolution.Width - 1) * stepSize;
            var height = (resolution.Height - 1) * stepSize;


            if (zoom != 1)
            {
                // We now know the default area based on the resolution and aspect ratio.
                // However, we now need to reduce this area based on the zoom factor.

                // Firstly we subtract the left edge from the centre and divide that by the zoom factor.
                // This gives us the desired distance from the centre to the left and right.
                var halfRealWidth = (centre.Real - minReal) / zoom;

                // Next, we subtract this amount from the centre to get the new left edge.
                minReal = centre.Real - halfRealWidth;

                // Similarly, we subtract the centre from the top and divide that by the zoom factor.
                // This gives us the desired distance from the centre to the top and bottom.
                var halfImagHeight = (maxImag - centre.Imaginary) / zoom;

                // Finally, we add this new amount to the centre to give us the new top edge.
                maxImag = (centre.Imaginary + halfImagHeight);

                width = width / zoom;
                height = height / zoom;
            }

            var area = new ComplexRectangle(minReal, maxImag, width, height);
            return area;
        }

        private static double GetStepSize(Size resolution, SizeF bounds)
        {
            var defaultAspectRatio = Generator.DefaultBounds.Width / Generator.DefaultBounds.Height;
            var selectedAspectRatio = resolution.Width / (double)resolution.Height;

            var stepSize = 0.0;

            if (selectedAspectRatio < defaultAspectRatio)
            {
                // Match width
                stepSize = Generator.DefaultBounds.Width / resolution.Width;
            }
            else
            {
                // Match height
                stepSize = Generator.DefaultBounds.Height / resolution.Height;
            }
            return stepSize;
        }

        private Complex ParseCentre(string text)
        {
            text = text.StartsWith("(") ? text.Substring(1) : text;
            text = text.EndsWith(")") ? text.Substring(0, text.Length - 1) : text;
            var textParts = text.Split(new char[] { ',' });

            double x = float.Parse(textParts[0]);
            double y = float.Parse(textParts[1]);
            return new Complex(x, y);
        }

        private Size ParseResolution(string text)
        {
            var textParts = text.Split(new char[] { 'x' });
            int w = int.Parse(textParts[0]);
            int h = int.Parse(textParts[1]);
            return new Size(w, h);
        }

        private void Generator_Progress(object sender, EventArgs e)
        {
            //Action progressAction = () => IncrementProgress();
            //this.BeginInvoke(progressAction);
        }

        private void IncrementProgress()
        {
            this.progressBar1.Increment(1);
        }

        private async Task ProcessResults(GeneratorSettings generatorSettings, PixelInfo[,] resultData)
        {
            this.progressBar1.Visible = false;

            OutputForm form = new OutputForm();
            await form.LoadData(generatorSettings, resultData);
            form.Show(this);
        }


    }
}
