﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MandelbrotGenerator.UI

{
    public partial class ColourWheelForm : Form
    {
        public ColourWheelForm()
        {
            InitializeComponent();
        }

        private void colourWheel1_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateStatusLabel(e.Location);
        }

        private void colourWheel1_MouseLeave(object sender, EventArgs e)
        {
            UpdateStatusLabel(Point.Empty);
        }

        private void UpdateStatusLabel(Point p)
        {
            if (p == Point.Empty)
            {
                this.toolStripStatusLabel1.Text = "Ready";
            }
            else
            {
                var child = this.colourWheel1.GetChildAtPoint(p);

                bool colourFound = false;

                if (child is PictureBox)
                {
                    var pixel = ((child as PictureBox).Image as Bitmap).GetPixel(p.X, p.Y);
                    if ((pixel.R == 0) && (pixel.G == 0) && (pixel.B == 0))
                    {

                    }
                    else
                    {
                        this.toolStripStatusLabel1.Text = string.Format("R={0:000}, G={1:000}, B={2:000}", pixel.R, pixel.G, pixel.B);
                        colourFound = true;
                    }
                }

                if (!colourFound)
                {
                    this.toolStripStatusLabel1.Text = "Ready";
                }
            }
        }
    }
}
