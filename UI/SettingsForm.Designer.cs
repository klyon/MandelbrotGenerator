﻿namespace MandelbrotGenerator.UI
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.centreLabel = new System.Windows.Forms.Label();
            this.centreTextBox = new System.Windows.Forms.TextBox();
            this.zoomTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.generateButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.maxIterationsTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.resolutionComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // centreLabel
            // 
            this.centreLabel.AutoSize = true;
            this.centreLabel.Location = new System.Drawing.Point(12, 15);
            this.centreLabel.Name = "centreLabel";
            this.centreLabel.Size = new System.Drawing.Size(38, 13);
            this.centreLabel.TabIndex = 0;
            this.centreLabel.Text = "Centre";
            // 
            // centreTextBox
            // 
            this.centreTextBox.Location = new System.Drawing.Point(110, 12);
            this.centreTextBox.Name = "centreTextBox";
            this.centreTextBox.Size = new System.Drawing.Size(157, 20);
            this.centreTextBox.TabIndex = 1;
            this.centreTextBox.Text = "(-0.5, 0)";
            // 
            // zoomTextBox
            // 
            this.zoomTextBox.Location = new System.Drawing.Point(110, 38);
            this.zoomTextBox.Name = "zoomTextBox";
            this.zoomTextBox.Size = new System.Drawing.Size(54, 20);
            this.zoomTextBox.TabIndex = 3;
            this.zoomTextBox.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Zoom";
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(102, 117);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(75, 23);
            this.generateButton.TabIndex = 8;
            this.generateButton.Text = "Generate";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Max Iterations";
            // 
            // maxIterationsTextBox
            // 
            this.maxIterationsTextBox.Location = new System.Drawing.Point(110, 91);
            this.maxIterationsTextBox.Name = "maxIterationsTextBox";
            this.maxIterationsTextBox.Size = new System.Drawing.Size(54, 20);
            this.maxIterationsTextBox.TabIndex = 7;
            this.maxIterationsTextBox.Text = "360";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Output Resolution";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 146);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(255, 23);
            this.progressBar1.TabIndex = 9;
            this.progressBar1.Visible = false;
            // 
            // resolutionComboBox
            // 
            this.resolutionComboBox.DisplayMember = "Key";
            this.resolutionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionComboBox.FormattingEnabled = true;
            this.resolutionComboBox.Location = new System.Drawing.Point(110, 64);
            this.resolutionComboBox.Name = "resolutionComboBox";
            this.resolutionComboBox.Size = new System.Drawing.Size(157, 21);
            this.resolutionComboBox.TabIndex = 5;
            this.resolutionComboBox.ValueMember = "Value";
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.generateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 181);
            this.Controls.Add(this.resolutionComboBox);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.maxIterationsTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.zoomTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.centreTextBox);
            this.Controls.Add(this.centreLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ken\'s Mandelbrot Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label centreLabel;
        private System.Windows.Forms.TextBox centreTextBox;
        private System.Windows.Forms.TextBox zoomTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox maxIterationsTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ComboBox resolutionComboBox;
    }
}

