﻿namespace MandelbrotGenerator.UI
{
    partial class OutputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.outputPictureBox = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.saveToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.zoomToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.filledPanel = new System.Windows.Forms.Panel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.outputPictureBox)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.filledPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // outputPictureBox
            // 
            this.outputPictureBox.ContextMenuStrip = this.contextMenuStrip1;
            this.outputPictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.outputPictureBox.Location = new System.Drawing.Point(0, 0);
            this.outputPictureBox.Name = "outputPictureBox";
            this.outputPictureBox.Size = new System.Drawing.Size(280, 280);
            this.outputPictureBox.TabIndex = 0;
            this.outputPictureBox.TabStop = false;
            this.outputPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.outputPictureBox_Paint);
            this.outputPictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.outputPictureBox_MouseDoubleClick);
            this.outputPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.outputPictureBox_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveImageToolStripMenuItem,
            this.resolutionToolStripMenuItem,
            this.zoomInToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(135, 70);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.Image = global::MandelbrotGenerator.Properties.Resources.saveHS;
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.saveImageToolStripMenuItem.Text = "&Save Image";
            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.saveImageToolStripMenuItem_Click);
            // 
            // resolutionToolStripMenuItem
            // 
            this.resolutionToolStripMenuItem.Name = "resolutionToolStripMenuItem";
            this.resolutionToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.resolutionToolStripMenuItem.Text = "Resolution";
            // 
            // zoomInToolStripMenuItem
            // 
            this.zoomInToolStripMenuItem.Image = global::MandelbrotGenerator.Properties.Resources.ZoomHS;
            this.zoomInToolStripMenuItem.Name = "zoomInToolStripMenuItem";
            this.zoomInToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.zoomInToolStripMenuItem.Text = "&Zoom In";
            this.zoomInToolStripMenuItem.Click += new System.EventHandler(this.zoomInToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripSplitButton,
            this.zoomToolStripSplitButton,
            this.toolStripStatusLabel,
            this.toolStripProgressBar1});
            this.statusStrip.Location = new System.Drawing.Point(0, 285);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(285, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 1;
            // 
            // saveToolStripSplitButton
            // 
            this.saveToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripSplitButton.Image = global::MandelbrotGenerator.Properties.Resources.saveHS;
            this.saveToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripSplitButton.Name = "saveToolStripSplitButton";
            this.saveToolStripSplitButton.Size = new System.Drawing.Size(32, 20);
            this.saveToolStripSplitButton.ButtonClick += new System.EventHandler(this.saveToolStripSplitButton_ButtonClick);
            // 
            // zoomToolStripSplitButton
            // 
            this.zoomToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomToolStripSplitButton.Image = global::MandelbrotGenerator.Properties.Resources.ZoomHS;
            this.zoomToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomToolStripSplitButton.Name = "zoomToolStripSplitButton";
            this.zoomToolStripSplitButton.Size = new System.Drawing.Size(32, 20);
            this.zoomToolStripSplitButton.ButtonClick += new System.EventHandler(this.zoomToolStripSplitButton_ButtonClick);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Ready";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar1.Visible = false;
            // 
            // filledPanel
            // 
            this.filledPanel.AutoScroll = true;
            this.filledPanel.Controls.Add(this.outputPictureBox);
            this.filledPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filledPanel.Location = new System.Drawing.Point(0, 0);
            this.filledPanel.Name = "filledPanel";
            this.filledPanel.Size = new System.Drawing.Size(285, 285);
            this.filledPanel.TabIndex = 2;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "*.bmp";
            this.saveFileDialog.Filter = "PNG Images (*.png) |*.png|Bitmap Images (*.bmp) |*.bmp";
            // 
            // OutputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 307);
            this.Controls.Add(this.filledPanel);
            this.Controls.Add(this.statusStrip);
            this.Name = "OutputForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Output";
            ((System.ComponentModel.ISupportInitialize)(this.outputPictureBox)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.filledPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox outputPictureBox;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripSplitButton saveToolStripSplitButton;
        private System.Windows.Forms.Panel filledPanel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton zoomToolStripSplitButton;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem zoomInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resolutionToolStripMenuItem;
    }
}