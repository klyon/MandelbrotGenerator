﻿using System;
using System.Drawing;

namespace MandelbrotGenerator
{
    public static class ExtensionMethods
    {
        public static string GetAspectRatio(this Size size)
        {
            var xValue = 0.0;
            var yValue = 0.0;

            if (size.Width > size.Height)
            {
                xValue = size.Width / (double)size.Height;
                yValue = 1;
            }
            else
            {
                xValue = 1;
                yValue = size.Height / (double)size.Width;
            }

            int multiplier = 2;

            while (xValue != Math.Floor(xValue) || yValue != Math.Floor(yValue))
            {
                var newX = xValue * multiplier;
                var newY = yValue * multiplier;

                if (newX == Math.Floor(newX) && newY == Math.Floor(newY))
                {
                    xValue = newX;
                    yValue = newY;
                }
                else
                {
                    multiplier++;
                }
            }

            return String.Format("{0}:{1}", xValue, yValue);
        }
    }
}