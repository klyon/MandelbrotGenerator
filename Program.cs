﻿using MandelbrotGenerator.BusinessLogic;
using MandelbrotGenerator.Model;
using MandelbrotGenerator.UI;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MandelbrotGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new BrowserForm());

            //var generatorSettings = new GeneratorSettings();
            //generatorSettings.Area = Geometry.GetComplexArea(generatorSettings);
            //var generator = new Generator();
            //generator.GenerateSet(generatorSettings, false);
            //var form = new OutputForm();
            //form.LoadData(generatorSettings, generator.Result, false);
            //Application.Run(form);
        }


    }
}
